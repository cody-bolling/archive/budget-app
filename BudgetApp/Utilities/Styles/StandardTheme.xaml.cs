﻿using Xamarin.Forms.Xaml;

namespace BudgetApp.Utilities.Styles
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StandardTheme
    {
        public StandardTheme()
        {
            InitializeComponent();
        }
    }
}
