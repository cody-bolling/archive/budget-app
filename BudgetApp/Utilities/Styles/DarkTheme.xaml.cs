﻿using Xamarin.Forms.Xaml;

namespace BudgetApp.Utilities.Styles
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DarkTheme
    {
        public DarkTheme()
        {
            InitializeComponent();
        }
    }
}
