using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("Roboto-Light.ttf", Alias = "Roboto-Light")]
[assembly: ExportFont("Roboto-Medium.ttf", Alias = "Roboto-Medium")]